package emmanuelleyouranjavaiiiproject.sorts;
import java.util.Comparator;
import emmanuelleyouranjavaiiiproject.types.Electronics;

/**
 * SortByBrand implements comparator
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public class SortByBrand implements Comparator<Electronics>{
    @Override
    /**
     * compare compares 2 electronics for the purpose of sorting
     * 
     * @param o1 Electronic to compare
     * @param 02 Electonic to compare to
     */
    public int compare(Electronics o1, Electronics o2) {
        return o1.getBrand().compareTo(o2.getBrand());
    }
}