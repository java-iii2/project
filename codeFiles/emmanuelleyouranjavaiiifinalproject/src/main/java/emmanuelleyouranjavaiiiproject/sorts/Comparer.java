package emmanuelleyouranjavaiiiproject.sorts;
import emmanuelleyouranjavaiiiproject.types.Electronics;

/**
 * The Comparer class contains methods for comparing Electronics objects based on different strategies.
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public class Comparer {
    public String strategy;
    public String direction;

    /**
     * Compares two Electronics objects based on the selected strategy and direction.
     *
     * @param e1 The first Electronics object to compare
     * @param e2 The second Electronics object to compare
     * @return An integer representing the comparison result
     */
    public int compare(Electronics e1, Electronics e2){
        if (strategy.equals("price")) {
            if (direction.equals("asc")) {
                return priceCompareAsc(e1.getPrice(), e2.getPrice());
            }
            else if(direction.equals("desc")){
                return priceCompareDesc(e1.getPrice(), e2.getPrice());
            }
        }else if(strategy.equals("name")){
            if (direction.equals("asc")) {
                return nameCompareAsc(e1.getModel_or_Name(), e2.getModel_or_Name());
            }
            else if(direction.equals("desc")){
                return nameCompareDesc(e1.getModel_or_Name(), e2.getModel_or_Name());
            }
        }
        return 0;
    }

    /**
     * Compares prices in ascending order.
     *
     * @param price1 The first price to compare
     * @param price2 The second price to compare
     * @return An integer representing the comparison result for ascending order
     */
    public int priceCompareAsc(double price1, double price2){
        if ((price1 - price2) > 0) {
            return 1;
        }
        if ((price1 - price2) < 0){
            return -1;
        }
        return 0;
    }

    /**
     * Compares prices in descending order.
     *
     * @param price1 The first price to compare
     * @param price2 The second price to compare
     * @return An integer representing the comparison result for descending order
     */
    public int priceCompareDesc(double price1, double price2){
        if ((price1 - price2) > 0) {
            return -1;
        }
        if ((price1 - price2) < 0){
            return 1;
        }
        return 0;
    }

    /**
     * Compares names in ascending order.
     *
     * @param name1 The first name to compare
     * @param name2 The second name to compare
     * @return An integer representing the comparison result for ascending order
     */
    public int nameCompareAsc(String name1, String name2){
        return name1.compareTo(name2);
    }

    /**
     * Compares names in descending order.
     *
     * @param name1 The first name to compare
     * @param name2 The second name to compare
     * @return An integer representing the comparison result for descending order
     */
    public int nameCompareDesc(String name1, String name2){
        return -1 * name1.compareTo(name2);
    }
}
