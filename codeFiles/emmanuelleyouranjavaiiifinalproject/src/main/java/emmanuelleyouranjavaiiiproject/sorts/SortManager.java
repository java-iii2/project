package emmanuelleyouranjavaiiiproject.sorts;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import emmanuelleyouranjavaiiiproject.types.*;

/**
 * sortmanager manages the different sort strategies
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public class SortManager {
    /**
     * sortElectronic sorts the electronics depending on a direction and strategy
     * 
     * @param arrayOfElectronics the list of electronics to sort
     * @param direction the direction to sort in
     * @param strategy the thing that the comparator will use to sort the list
     */
    public List<Electronics> sortElectronics(List<Electronics> arrayOfElectronics, String direction, String strategy){
        Comparator<Electronics> comparer = null;

        switch (strategy) {
            case "price":
                comparer = direction.equals("asc") ? new SortByPriceAsc() : new SortByPriceDesc();
                break;
            case "name":
                comparer = direction.equals("asc") ? new SortByNameAsc() : new SortByPriceDesc();
                break;
            case "brand":
                comparer = new SortByBrand();
                break;
        }

        Collections.sort(arrayOfElectronics, comparer);
        return arrayOfElectronics;
    }
}
