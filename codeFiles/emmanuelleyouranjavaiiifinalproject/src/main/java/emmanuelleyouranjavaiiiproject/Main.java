package emmanuelleyouranjavaiiiproject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import emmanuelleyouranjavaiiiproject.applications.Application;

/**
 * Main runs all the function in the program using loginOrCreate
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public class Main {
    /**
     * main method
     */
    public static void main(String[] args) {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection conn = null;
        try{
            conn = DriverManager.getConnection(url, "A2239468", "1L0v3cr0ch3t1ng");

        } catch(SQLException e){
            e.printStackTrace();
        }
        Application app = new Application();
        app.loginOrCreate(conn);
    }
}
