package emmanuelleyouranjavaiiiproject.types;

/**
 * Phone which extends Electronics
 * 
 * @author You Ran Wang, Emmanuelle Lin
 * @version 2023/11/14
 */
public class Phone extends Electronics{
    private int storage;
    private String color;
    private String size;

    /**
     * constructor for Phone
     * @param price
     * @param brand
     * @param model_or_name
     * @param storage
     * @param color
     * @param size
     */
    public Phone(double price, String brand, String model_or_name, int storage, String color, String size){
        super(price, brand, model_or_name);
        this.storage = storage;
        this.color = color;
        this.size = size;
    }

    /**
     * getter for storage
     * @return double storage
     */
    public int getStorage(){
        return this.storage;
    }

    /**
     * getter for color
     * @return String color
     */
    public String getColor(){
        return this.color;
    }

    /**
     * getter for size
     * @return String size
     */
    public String getSize(){
        return this.size;
    }

    /**
     * setter for storage
     * @param storage
     */
    public void setStorage(int storage){
        this.storage = storage;
    }

    /**
     * setter for color
     * @param color
     */
    public void setColor(String color){
        this.color = color;
    }

    /**
     * setter for size
     * @param size
     */
    public void setSize(String size){
        this.size = size;
    }

    /**
     * toString for Phone that prints all the fields in its class and its base class
     * @return String the Phone and its specs
     */
    public String toString(){
        return "Price: " + super.getPrice() +", Brand: " + super.getBrand() + ", Model or Name: " + getModel_or_Name() + ", Storage: " + getStorage() + "GB, Color: " + getColor() + ", Size: " + getSize();
    }
}
