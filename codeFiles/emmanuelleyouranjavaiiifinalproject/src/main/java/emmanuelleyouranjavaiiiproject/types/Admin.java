package emmanuelleyouranjavaiiiproject.types;

import java.sql.*;
import java.util.Map;

/**
 * Admin represents a manager who's a user but receives double points
 * @author Emmanuelle Lin
 * @version 2023/12/01
 */
public class Admin extends User{
    public String typeName;

    /**
     * constructor for Admin
     * @param usn
     * @param pwd
     * @param pts
     */
    public Admin(String usn, String pwd, int pts){
        super(usn, pwd, pts);
        this.typeName = "ADMIN_TYPE";
    }
    public Admin(String usn, String pwd){
        super(usn, pwd);
        this.typeName = "ADMIN_TYPE";
    }

    /**
     * displayUsers displays all the users and their points without showing the password
     * @param conn - connection to database
     */
    public void displayUsers(Connection conn){
        try {
            Map<String, Class<?>> map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("ADMIN_TYPE", Class.forName("emmanuelleyouranjavaiiiproject.types.Admin"));

            PreparedStatement stmt = conn.prepareStatement("SELECT username, points, managerOrNot FROM UserManager");
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                System.out.println(result.getString("username") + "  " + result.getInt("points") + "  " + result.getInt("managerOrNot"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Override for Admin class
     */
    @Override
    public void createUser(Connection conn, boolean newOrNot){
        try{
            Map<String, Class<?>> map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("USER_TYPE", Class.forName("emmanuelleyouranjavaiiiproject.types.Admin"));

            PreparedStatement stmt = conn.prepareStatement("INSERT INTO UserManager VALUES(?,?,?,?)");
            stmt.setString(1, super.getUserName());
            stmt.setString(2, super.getPassword());
            if(newOrNot){
                stmt.setInt(3, 200);
                System.out.println("You now have 200 points in your account");
            } else{
                stmt.setInt(3, this.getPoints());
            }
            stmt.setBoolean(4, true);

            stmt.executeUpdate();

        } catch(Exception e){
            e.getStackTrace();
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param conn - connection to database
     * @param userToChange - the user where whatever change should be made
     * @param columnToChange - which column to change (username, password, or points)
     * @param numToReplace - the replacement for points
     * @param replace - the replacement for the rest
     */
    public void updateUser(Connection conn, String userToChange, int columnToChange, int numToReplace, String replace){
        try {
            Map<String, Class<?>> map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("USER_TYPE", Class.forName("emmanuelleyouranjavaiiiproject.types.Admin"));

            String columnToUpdate = "";
            switch (columnToChange) {
                case 1:
                    columnToUpdate = "username";
                    break;
                case 2:
                    columnToUpdate = "password";
                    break;
                case 3:
                    columnToUpdate = "points";
                    break;
            }

            String updateQuery = "UPDATE UserManager SET " + columnToUpdate + " = ? WHERE username = ?";

            PreparedStatement stmt = conn.prepareStatement(updateQuery);

            if (replace.equals("")) {
                stmt.setInt(1, numToReplace);
            } else {
                stmt.setString(1, replace);
            }
            stmt.setString(2, userToChange);

            stmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * deleteUser takes the username of the user and delete the account
     * @param conn
     * @param username
     */
    public void deleteUser(Connection conn, String username){
        try {
            Map<String, Class<?>> map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("USER_TYPE", Class.forName("emmanuelleyouranjavaiiiproject.types.Admin"));

            PreparedStatement stmt = conn.prepareStatement("DELETE FROM UserManager WHERE username = ?");
            stmt.setString(1, username);
            stmt.executeUpdate();

        } catch (Exception e) {
            System.out.println("User does not exist");
            e.printStackTrace();
        }
    }

    /**
     * twoTimesPoints takes new points*2 and adds it to current points
     * @param newPoints - points from new purchase
     * @return int total points after adding old points
     */
    public void twoTimesPoints(Connection conn, Admin admin){
        super.setPointsToDB(conn, getPointsFromDB(conn, admin.getUserName()) + 100, admin.getUserName());
    }

    /**
     * toString for Admin that prints all the fields its base class
     */
    @Override
    public String toString(){
        return super.toString();
    }
}
