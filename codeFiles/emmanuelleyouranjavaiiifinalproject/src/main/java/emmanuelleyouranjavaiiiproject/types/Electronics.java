package emmanuelleyouranjavaiiiproject.types;

/**
 * Base class for all products in data
 * 
 * @author You Ran Wang
 * @version 2023/11/08
 */
public abstract class Electronics{
    private double price;
    private String brand;
    private String model_or_name;

    /**
     * constructor for Electronics
     * 
     * @param price
     * @param brand
     * @param model_or_name
     */
    public Electronics(double price, String brand, String model_or_name){
        this.price = price;
        this.brand = brand;
        this.model_or_name = model_or_name;
    }

    /**
     * getter for price
     * 
     * @return double price
     */
    public double getPrice(){
        return this.price;
    }


    /**
     * getter for brand
     * 
     * @return String brand
     */
    public String getBrand(){
        return this.brand;
    }

    
    /**
     * getter for the model or name
     * 
     * @return
     */
    public String getModel_or_Name(){
        return this.model_or_name;
    }


    /**
     * setter for price
     * 
     * @param price
     */
    public void setPrice(double price){
        this.price = price;
    }


    /**
     * setter for brand
     * 
     * @param brand
     */
    public void setBrand(String brand){
        this.brand = brand;
    }


    /**
     * setter for model or name
     * 
     * @param model_or_name
     */
    public void setModel_or_Name(String model_or_name){
        this.model_or_name = model_or_name;
    }
    

}
