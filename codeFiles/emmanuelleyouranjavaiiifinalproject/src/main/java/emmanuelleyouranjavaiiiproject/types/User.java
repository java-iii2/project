package emmanuelleyouranjavaiiiproject.types;

import java.sql.*;
import java.util.Map;

/**
 * @author Emmanuelle Lin
 * @version 2023/11/29
 */
public class User implements SQLData{

    private String userName;
    private String password;
    private int points;
    public String typeName;

    /**
     * Constructor for User
     * @param usn
     * @param pwd
     * @param pts
     */
    public User(String usn, String pwd, int pts){
        this.userName = usn;
        this.password = pwd;
        this.points = pts;
        this.typeName = "USER_TYPE";
    }
    public User(String usn, String pwd){
        this.userName = usn;
        this.password = pwd;
        this.points = 0;
        this.typeName = "USER_TYPE";
    }

    /**
     * getter for username
     * @return String username
     */
    public String getUserName() {
        return this.userName;
    }
    /**
     * getter for password
     * @return String password
     */
    public String getPassword() {
        return this.password;
    }
    /**
     * getter for points
     * @return int points
     */
    public int getPoints() {
        return this.points;
    }
    
    /**
     * setter for username
     * @param username
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * setter for password
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }
    /**
     * setter for points
     * @param points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * createUser takes the username and password and pushes it to the database table
     * @param conn - connection to PDBORAC
     * @param usn - username
     * @param pwd - password
     */
    public void createUser(Connection conn, boolean newOrNot){
        try{
            Map<String, Class<?>> map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("USER_TYPE", Class.forName("emmanuelleyouranjavaiiiproject.types.User"));

            PreparedStatement stmt = conn.prepareStatement("INSERT INTO UserManager VALUES(?,?,?,?)");
            stmt.setString(1, this.getUserName());
            stmt.setString(2, this.getPassword());
            if(newOrNot){
                stmt.setInt(3, 100);
                System.out.println("You now have 100 points in your account");
            } else{
                stmt.setInt(3, this.getPoints());
            }
            stmt.setBoolean(4, false);

            stmt.executeUpdate();

        } catch(Exception e){
            e.getStackTrace();
            e.printStackTrace();
        }
    }

    /**
     * validateUser validates username and password
     * @param conn - connection to PDBORAC
     * @param username - username
     * @param password - password
     * @return String Login failed or Login successful
     */
    public boolean validateUser(Connection conn, String username, String password){

        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT password FROM UserManager WHERE username = ?");
            stmt.setString(1, username);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                if(result.getString(1).equals(password)){
                    System.out.println("\n=====Login successful===== \n\n");
                    return true;
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\n=====Login failed=====\n\n");
        return false; 
    }

    public boolean checkAdmin(Connection conn, String username){

        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT managerOrNot FROM UserManager WHERE username = ?");
            stmt.setString(1, username);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                if(result.getInt(1) == 1){
                    System.out.println("\n===== WELCOME ADMIN " + username + " ===== \n\n");
                    return true;
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\n===== WELCOME USER " + username + " ===== \n\n");
        return false; 
    }

    /**
     * getPoints gets the points associated to the user from the database 
     * @param conn - connection to database
     * @param username - the username of the user's points to be returned
     * @return - int points to be returned
     */
    public int getPointsFromDB(Connection conn, String username){
        int userPoints = 0;
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT points FROM UserManager WHERE username = ?");
            stmt.setString(1, username);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                userPoints = result.getInt("points");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userPoints;
    }

    public void setPointsToDB(Connection conn, int points, String username){
        try {
            PreparedStatement stmt = conn.prepareStatement("UPDATE UserManager SET points = ? WHERE username = ?");
            stmt.setInt(1, points);
            stmt.setString(2, username);

            stmt.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    @Override
    public String getSQLTypeName() throws SQLException {
        return typeName;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.typeName = typeName;
        setUserName(stream.readString());
        setPassword(stream.readString());
        setPoints(stream.readInt());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getUserName());
        stream.writeString(getPassword());
        stream.writeInt(getPoints());
    }
}
