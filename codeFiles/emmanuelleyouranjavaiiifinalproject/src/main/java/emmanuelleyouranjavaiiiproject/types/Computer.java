package emmanuelleyouranjavaiiiproject.types;

/**
 * Computer class which extends electronics
 * 
 * @author You Ran Wang
 * @version 2023/11/08
 */
public class Computer extends Electronics{
    private int storage;
    private String cpu;
    private String gpu;
    private int memory;
    private String psu;
    private String form_factor;
    private String motherboard;

    /**
     * constructor for computer
     * 
     * @param price
     * @param brand
     * @param model_or_name
     * @param storage
     * @param cpu
     * @param gpu
     * @param memory
     * @param psu
     * @param form_factor
     * @param motherboard
     */
    public Computer(double price, String brand, String model_or_name, int storage, String cpu, String gpu, int memory, String psu, String form_factor, String motherboard) {
        super(price, brand, model_or_name);
        this.storage = storage;
        this.cpu = cpu;
        this.gpu = gpu;
        this.memory = memory;
        this.psu = psu;
        this.form_factor = form_factor;
        this.motherboard = motherboard;
    }

    /**
     * setter for storage
     * 
     * @param storage
     */
    public void setStorage(int storage) {
        this.storage = storage;
    }

    /**
     * setter for cpu
     * 
     * @param cpu
     */
    public void setCpu(String cpu) {
        this.cpu = cpu;
    }


    /**
    * setter for gpu
    * 
    * @param gpu
    */
    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    /**
    * setter for memory
    * 
    * @param memory
    */
    public void setMemory(int memory) {
        this.memory = memory;
    }


    /**
    * setter for psu
    * 
    * @param psu
    */
    public void setPsu(String psu) {
        this.psu = psu;
    }

    /**
    * setter for form factor
    * 
    * @param form_factor
    */
    public void setForm_factor(String form_factor) {
        this.form_factor = form_factor;
    }


    /**
    * setter for motherboard
    * 
    * @param motherboard
    */
    public void setMotherboard(String motherboard) {
        this.motherboard = motherboard;
    }


    /**
    * getter for storage
    * 
    * @return int storage
    */
    public int getStorage() {
        return storage;
    }


    /**
    * getter for cpu
    * 
    * @return String cpu
    */
    public String getCpu() {
        return cpu;
    }

    /**
    * getter for gpu
    * 
    * @return String gpu
    */
    public String getGpu() {
        return gpu;
    }


    /**
    * getter for memory
    * 
    * @return int memory
    */
    public int getMemory() {
        return memory;
    }

    /**
    * getter for psu
    * 
    * @return String psu
    */
    public String getPsu() {
        return psu;
    }


    /**
    * getter for form factor
    * 
    * @return String form factor
    */
    public String getForm_factor() {
        return form_factor;
    }

    /**
    * getter for motherboard
    * 
    * @return String motherboard
    */
    public String getMotherboard() {
        return motherboard;
    }

    @Override
    /**
     * overide for the toString for the computer class
     * returns String which represents the computer
     */
    public String toString() {
        return "Price: " + this.getPrice() +
                ", Brand: " + this.getBrand() +
                ", Model or Name: " + this.getModel_or_Name() +
                ", Storage: " + storage +
                ", CPU: " + cpu +
                ", GPU: " + gpu +
                ", Memory: " + memory +
                ", PSU: " + psu +
                ", Form Factor: " + form_factor +
                ", Motherboard: " + motherboard;
    }
}
