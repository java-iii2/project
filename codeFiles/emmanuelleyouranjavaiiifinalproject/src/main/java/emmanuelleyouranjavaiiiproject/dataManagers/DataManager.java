package emmanuelleyouranjavaiiiproject.dataManagers;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import emmanuelleyouranjavaiiiproject.sorts.SortManager;
import emmanuelleyouranjavaiiiproject.types.Computer;
import emmanuelleyouranjavaiiiproject.types.Electronics;
import emmanuelleyouranjavaiiiproject.types.Phone;
import emmanuelleyouranjavaiiiproject.types.User;

/**
 * Data Manager class keeps all the functions related to data manipulation
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public class DataManager {
    /**
     * Offers options to the user for viewing products.
     *
     * @param data List of Electronics passed in while invoked
     */
    public void viewData(List<Electronics> data){
        int whatToDo = -1;
        Scanner keyboard = new Scanner(System.in);
        while (!(whatToDo >= 1 && whatToDo <= 4)) {
            try {
                System.out.println(
                        "What would you like to do? \n1)View all computers  \n2)View all phones  \n3)View all products \n4)View sorted products ( by price etc.) \n-------------------------");
                whatToDo = keyboard.nextInt();
            } catch (Exception e) {
                System.out.println("Try again. You must input a number between 1 and 4");
                keyboard.next();
            }
        }
        switch (whatToDo) {
            case 1:
                int computerCount = 0;
                for(int i = 0; i < data.size(); i++){
                    if (data.get(i) instanceof Computer) {
                        computerCount++;
                        System.out.println(computerCount + ": " + data.get(i) + " \n");
                    }
                }
                break;
            case 2:
                int phoneCount = 0;
                for(int i = 0; i < data.size(); i++){
                    if (data.get(i) instanceof Phone) {
                        phoneCount++;
                        System.out.println(phoneCount + ": " + data.get(i) + " \n");
                    }
                }
                break;
            case 3:
                showData(data);
                break;
            case 4:
                viewSortedData(data, keyboard);
                break;
        }

    }

    /**
     * Displays sorted data based on specified sorting parameters.
     *
     * @param data List of Electronics passed for sorting
     */
    public void viewSortedData(List<Electronics> data, Scanner keyboard){
        int priceSort = 69;
        int nameSort = 69;
        int brandsort = 69;
    
            while(priceSort < -1 || priceSort > 1 ){
                try {
                    System.out.println("How should the products price be sorted? \n 1)Ascending order  \n-1)Descending order  \n 0)Not sorted by price \n-------------------------");
                    priceSort = keyboard.nextInt();

                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and -1");
                    keyboard.next();
                }
            }
            while(nameSort < -1 || nameSort > 1){
                try {
                    System.out.println("How should the products name be sorted? \n 1)Ascending order  \n-1)Descending order  \n 0)Not sorted by name \n-------------------------");
                    nameSort = keyboard.nextInt();
                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and -1");
                    keyboard.next();
                }
            }
            while(brandsort < 0 || brandsort > 1){
                try {
                    System.out.println("How should the products brand be sorted? \n1)Ascending order  \n0)Not sorted by brand \n-------------------------");
                    brandsort = keyboard.nextInt();
                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and 0");
                    keyboard.next();
                }
            }
        
        SortManager sm = new SortManager();
        switch (priceSort) {
            case 1:
                // sort by ascending for prices
                sm.sortElectronics((ArrayList<Electronics>) data, "asc", "price");
                break;
            case -1:
                // sort by descending for prices
                sm.sortElectronics((ArrayList<Electronics>) data, "desc", "price");
                break;
            case 0:
                break;

        }

        switch (nameSort) {
            case 1:
                // sort by ascending for name
                sm.sortElectronics((ArrayList<Electronics>) data, "asc", "name");
                break;
            case -1:
                // sort by descending for name
                sm.sortElectronics((ArrayList<Electronics>) data, "desc", "name");
                break;
            case 0:
                break;

        }

        switch (brandsort) {
            case 1:
                sm.sortElectronics((ArrayList<Electronics>) data, "asc", "brand");
                break;
        
            case 0:
                break;
        }

        showData(data);
    }

    /**
     * Allows a user to purchase an electronic product from the available data.
     *
     * @param data List of Electronics containing available products
     * @param user User object making the purchase
     */
    public void buyElectronic(List<Electronics> data, User user, Connection conn){
        try (Scanner keyboard = new Scanner(System.in)) {
            showData(data);
            int itemToBuy = -69;
            while(!(itemToBuy > 0 && itemToBuy < data.size() + 1)){
                try {
                    System.out.println("Which product do you want to buy? Input a number corresponding to a product: ");
                    itemToBuy = keyboard.nextInt();
                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and " + data.size() + 1);
                    keyboard.next();
                }
            }
            System.out.println("You bought item #" + itemToBuy + " ! 100 Points was been added to your account.");
            data.remove(itemToBuy-1);
            
            user.setPoints(user.getPointsFromDB(conn, user.getUserName()) + 100);
            user.setPointsToDB(conn, user.getPoints(), user.getUserName());

            IwriteProducts computerwrite = new IOwriteComputer();
            computerwrite.writeElectronics(data);
            IwriteProducts phonewrite = new IOwritePhone();
            phonewrite.writeElectronics(data);
        }
    }

    /**
     * Displays the list of electronic products.
     *
     * @param data List of Electronics to display
     */
    public void showData(List<Electronics> data){
        for(int i = 0; i < data.size(); i++){
            System.out.println(i + 1 + ": " + data.get(i) + " \n");
        }
    }

    /**
     * Allows editing options for a product.
     *
     * @param data List of Electronics to choose from
     */
    public void editProductOptions(List<Electronics> data, Scanner keyboard){
            showData(data);
            int changeTarget = -69;
            while(changeTarget < 0 || changeTarget > data.size() + 1){
                try {
                    System.out.println("Which product do you want to edit? Input a number corresponding to a product: ");
                    changeTarget = keyboard.nextInt();
                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and the last electronic");
                    keyboard.next();
                }
            }
            int whatToDo = -69;
            while(!(whatToDo == 1 || whatToDo == 2)){
                try {
                    System.out.println("What do you want to do? \n1)Delete \n2)Edit Price");
                    whatToDo = keyboard.nextInt();
                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and 2");
                    keyboard.next();
                }
            }

            switch (whatToDo) {
                case 1:
                    deleteData(changeTarget - 1, data);
                    break;
                case 2:
                    modifyData(changeTarget - 1, data);
                    break;
            }
        
    }

    /**
     * Deletes a product from the data.
     *
     * @param deleteTarget Index of the product to be deleted
     * @param data List of Electronics from which product will be deleted
     */
    public void deleteData(int deleteTarget, List<Electronics> data){
        System.out.println("You deleted item #" + deleteTarget + ".");
        data.remove(deleteTarget);
        IwriteProducts computerwrite = new IOwriteComputer();
        computerwrite.writeElectronics(data);
        IwriteProducts phonewrite = new IOwritePhone();
        phonewrite.writeElectronics(data);
    }

    /**
     * Modifies product details such as price.
     *
     * @param changeTarget Index of the product to be modified
     * @param data List of Electronics containing the product to be modified
     */
    public void modifyData(int changeTarget, List<Electronics> data){
        int change = -1000;
        try (Scanner keyboard = new Scanner(System.in)) {
            boolean validInput = false;
            while(!validInput)
            try {
                System.out.println("Input the change of price: (other fields cannot be modified and must be deleted and re-added if a mistake was made while adding the product)");
                change = keyboard.nextInt();
                validInput = true;
            } catch (Exception e) {
                System.out.println("Try again. You must input a number");
                keyboard.next();
            }
        }
        System.out.println("You changed item #" + changeTarget + ".");
        data.get(changeTarget).setPrice((double) change);
        IwriteProducts computerwrite = new IOwriteComputer();
        computerwrite.writeElectronics(data);
        IwriteProducts phonewrite = new IOwritePhone();
        phonewrite.writeElectronics(data);
    }

    /**
     * Offers options for adding new products to the existing data.
     *
     * @param data List of Electronics to which new products can be added
     */
    public void addProductOptions(List<Electronics> data){
         try (Scanner keyboard = new Scanner(System.in)) {
            showData(data);
            int addType = -69;
            while(!(addType == 1 || addType == 2)){
                try {
                    System.out.println("What type of product do you want to add? \n1)Computers \n2)Phones ");
                    addType = keyboard.nextInt();
                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and 2");
                    keyboard.next();
                }
            }

            switch (addType) {
                case 1:
                    addComputer(data);
                    break;
                case 2:
                    addPhone(data);
                    break;
            }
        }
    }

     /**
     * Adds a new Computer product to the data.
     *
     * @param data List of Electronics to add the new Computer product
     */
    public void addComputer(List<Electronics> data){
        try (Scanner keyboard = new Scanner(System.in)) {
            System.out.print("Enter price: ");
            int price = keyboard.nextInt();
            keyboard.nextLine(); // Consume the newline character

            System.out.print("Enter brand: ");
            String brand = keyboard.nextLine();

            System.out.print("Enter model/name: ");
            String model_or_name = keyboard.nextLine();


            System.out.print("Enter storage capacity (in GB): ");
            int storage = keyboard.nextInt();
            keyboard.nextLine(); // Consume the newline character

            System.out.print("Enter CPU model: ");
            String cpu = keyboard.nextLine();

            System.out.print("Enter GPU model: ");
            String gpu = keyboard.nextLine();

            System.out.print("Enter memory (in GB or MB): ");
            int memory = keyboard.nextInt();
            keyboard.nextLine(); // Consume the newline character

            System.out.print("Enter PSU model: ");
            String psu = keyboard.nextLine();

            System.out.print("Enter form factor: ");
            String formFactor = keyboard.nextLine();

            System.out.print("Enter motherboard model: ");
            String motherboard = keyboard.nextLine();
            
            data.add(new Computer(price, brand, model_or_name, storage, cpu, gpu, memory, psu, formFactor, motherboard));
            System.out.println("Added a new computer as product number " + data.size() + "!");
            IwriteProducts computerwrite = new IOwriteComputer();
            computerwrite.writeElectronics(data);
        }
    }

    /**
     * Adds a new Phone product to the data.
     *
     * @param data List of Electronics to add the new Phone product
     */
    public void addPhone(List<Electronics> data) {
        try (Scanner keyboard = new Scanner(System.in)) {
            System.out.print("Enter price: ");
            double price = keyboard.nextDouble();
            keyboard.nextLine(); // Consume the newline character

            System.out.print("Enter brand: ");
            String brand = keyboard.nextLine();

            System.out.print("Enter model/name: ");
            String modelOrName = keyboard.nextLine();

            System.out.print("Enter storage capacity (in GB): ");
            int storage = keyboard.nextInt();
            keyboard.nextLine(); // Consume the newline character

            System.out.print("Enter color: ");
            String color = keyboard.nextLine();

            System.out.print("Enter size: ");
            String size = keyboard.nextLine();

            data.add(new Phone(price, brand, modelOrName, storage, color, size));
            System.out.println("Added a new phone as product number " + data.size() + "!");
            IwriteProducts phonewrite = new IOwritePhone();
            phonewrite.writeElectronics(data);
        }
    }
}
