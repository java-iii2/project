package emmanuelleyouranjavaiiiproject.dataManagers;
import java.io.FileNotFoundException;
import java.nio.file.*;
import java.util.*;
import emmanuelleyouranjavaiiiproject.types.Computer;
import emmanuelleyouranjavaiiiproject.types.Electronics;
import emmanuelleyouranjavaiiiproject.types.Phone;

/**
 * filereader class will contain the classes neede to interact with txt files
 * @author You Ran Wang
 * @version 2023/11/14
 */
public class IOloadProducts implements IloadProducts{

    /**
     * loadElectronics loads computers from all the csv files
     * @return List<Electronics> of all products
     * @throws FileNotFoundException
     */
    public List<Electronics> loadElectronics(){
        try{
            List<Electronics> result = new ArrayList<Electronics>();
            Path p = Paths.get("codeFiles\\emmanuelleyouranjavaiiifinalproject\\src\\main\\data\\computers.csv");
            List<String> lines = Files.readAllLines(p);
            for(String line : lines){
                String[] oneline = line.split(",");
                result.add(new Computer(
                    Double.parseDouble(oneline[0]),
                    oneline[1],
                    oneline[2],
                    Integer.parseInt(oneline[3]),
                    oneline[4],
                    oneline[5],
                    Integer.parseInt(oneline[6]),
                    oneline[7],
                    oneline[8],
                    oneline[9]
                    )
                );
            }
            p = Paths.get("codeFiles\\emmanuelleyouranjavaiiifinalproject\\src\\main\\data\\phone.csv");
            lines = Files.readAllLines(p);
            for(String line : lines){
                String[] oneline = line.split(",");
                result.add(new Phone(
                    Double.parseDouble(oneline[0]),
                    oneline[1],
                    oneline[2],
                    Integer.parseInt(oneline[3]),
                    oneline[4],
                    oneline[5]
                    )
                );
            }
            return result;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}