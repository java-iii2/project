package emmanuelleyouranjavaiiiproject.dataManagers;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import emmanuelleyouranjavaiiiproject.types.Electronics;
import emmanuelleyouranjavaiiiproject.types.Phone;

/**
 * A class that will be used to contain loadElectronics for phones strategy
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public class IOwritePhone implements IwriteProducts{
    /**
     * write electronics writes specificcaly computers into a list
     * 
     * @param data is the list that will be written to a file
     */
    public void writeElectronics(List<Electronics> data){
        List<String> phones = new ArrayList<String>();
        for(int i = 0; i < data.size(); i++){
            if (data.get(i) instanceof Phone) {
                Phone phone = (Phone)data.get(i);
                phones.add(
                    phone.getPrice() + "," +
                    phone.getBrand() + "," +
                    phone.getModel_or_Name() + "," +
                    phone.getStorage() + "," +
                    phone.getColor() + "," +
                    phone.getSize()
                );
            }
        }
        try {
            Files.write(Paths.get("codeFiles\\emmanuelleyouranjavaiiifinalproject\\src\\main\\data\\phone.csv"), phones);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

