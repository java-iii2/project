package emmanuelleyouranjavaiiiproject.dataManagers;
import java.io.FileNotFoundException;
import java.util.*;
import emmanuelleyouranjavaiiiproject.types.Electronics;

/**
 * IloadProducts is an interface that can be implemented by different classes that can use different methods to laod products
 */
public interface IloadProducts {
    /**
     * loadElectronics uses any method to load a list of electronicss
     * @return List<Electronics> will be used as data in other functions
     * @throws FileNotFoundException
     */
    List<Electronics> loadElectronics() throws FileNotFoundException;
}
