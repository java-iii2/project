package emmanuelleyouranjavaiiiproject.dataManagers;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import emmanuelleyouranjavaiiiproject.types.Admin;
import emmanuelleyouranjavaiiiproject.types.User;

/**
 * PrelaodUsers is a class used to setup users in the sql database
 * 
 * @author Emmanuelle Lin
 * @version 2023/12/01
 */
public class PreloadedUsers {

    /**
     * loadUsers loads the users into the databse
     * 
     * @param conn the connection used to get to the database
     */
    public void loadUsers(Connection conn) {

        int count = 0;
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM UserManager");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (count == 0) {
            List<User> users = new ArrayList<User>();
            users.add(new User("Alpha123", "Pass123!", 567));
            users.add(new User("Beta456", "Secure789@", 234));
            users.add(new User("Gamma789", "Password567$", 891));
            users.add(new User("Delta234", "Secret432&", 123));
            users.add(new User("Epsilon567", "Hidden876*", 450));
            users.add(new User("Zeta890", "Confidential321#", 765));
            users.add(new User("Eta123", "Protected987%", 678));
            users.add(new User("Theta456", "Locked234!", 111));
            users.add(new User("Iota789", "Private567$", 333));
            users.add(new User("Kappa234", "Restricted890#", 999));
            users.add(new User("Lambda567", "Classified123@", 222));
            users.add(new User("Mu890", "TopSecret456$", 777));
            users.add(new User("Nu123", "UltraSecure789&", 888));
            users.add(new Admin("Xi456", "HiddenKey234*", 345));
            users.add(new Admin("Omicron789", "Access123#", 600));

            for (int i = 0; i < users.size(); i++) {
                users.get(i).createUser(conn, false);
            }
        }
    }
}
