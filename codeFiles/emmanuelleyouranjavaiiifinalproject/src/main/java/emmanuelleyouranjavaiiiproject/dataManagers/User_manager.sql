DROP TABLE UserManager;

CREATE TABLE UserManager(
    username        VARCHAR2(20)    NOT NULL   PRIMARY KEY,
    password        VARCHAR2(20)    NOT NULL,
    points          NUMBER(4)       NOT NULL
                                    CHECK(points > 0),
    managerOrNot    NUMBER(1)       NOT NULL
);