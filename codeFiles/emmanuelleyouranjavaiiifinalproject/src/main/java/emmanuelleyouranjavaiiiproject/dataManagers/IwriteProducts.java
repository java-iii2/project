package emmanuelleyouranjavaiiiproject.dataManagers;
import java.util.List;
import emmanuelleyouranjavaiiiproject.types.Electronics;

/**
 * IwriteProducts is a interface for containing different startegies to write to files
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public interface IwriteProducts {
    /**
     * writeElectronics will be a way that the program can write to files
     * 
     * @param data the list that will be written to file
     */
    void writeElectronics(List<Electronics> data);
}
