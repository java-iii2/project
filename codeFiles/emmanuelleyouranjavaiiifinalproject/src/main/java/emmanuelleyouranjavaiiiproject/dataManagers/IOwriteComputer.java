package emmanuelleyouranjavaiiiproject.dataManagers;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import emmanuelleyouranjavaiiiproject.types.Computer;
import emmanuelleyouranjavaiiiproject.types.Electronics;

/**
 * A class that will be used to contain loadElectronics for computer strategy
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public class IOwriteComputer implements IwriteProducts{
    /**
     * write electronics writes specificcaly computers into a list
     * 
     * @param data is the list that will be written to a file
     */
    public void writeElectronics(List<Electronics> data){
        List<String> computers=  new ArrayList<String>();
        for(int i = 0; i < data.size(); i++){
            if (data.get(i) instanceof Computer) {
                Computer computer = (Computer)data.get(i);
                computers.add(
                    computer.getPrice() + "," +
                    computer.getBrand() + "," +
                    computer.getModel_or_Name() + "," +
                    computer.getStorage() + "," +
                    computer.getCpu() + "," +
                    computer.getGpu() + "," +
                    computer.getMemory() + "," +
                    computer.getPsu() + "," +
                    computer.getForm_factor() + "," +
                    computer.getMotherboard()
                );
            }
        }
        try {
            Files.write(Paths.get("codeFiles\\emmanuelleyouranjavaiiifinalproject\\src\\main\\data\\computers.csv"), computers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
