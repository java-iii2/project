package emmanuelleyouranjavaiiiproject.applications;
import java.sql.*;
import java.util.*;
import emmanuelleyouranjavaiiiproject.dataManagers.DataManager;
import emmanuelleyouranjavaiiiproject.dataManagers.IOloadProducts;
import emmanuelleyouranjavaiiiproject.types.Electronics;
import emmanuelleyouranjavaiiiproject.types.User;
import emmanuelleyouranjavaiiiproject.types.Admin;

/**
 * Application is the application used by users and contains user's showOption
 */
public class Application {
    /**
     * showOptionshows the user all the things they can do and invokes the appropriate functions
     * 
     * @param data is a list of data to be used
     * @param user is the user using the application
     * @param conn is the connection that will be used to edit users and such in sql
     */
    public void showOptions(List<Electronics> data, User user, Connection conn){
        DataManager dataManager = new DataManager();
        try (Scanner keyboard = new Scanner(System.in)) {
            int whatToDo = 69420;
            System.out.println("\n== You have " + user.getPointsFromDB(conn, user.getUserName()) + " points :) ==\n");
            while(whatToDo < 1 || whatToDo > 2){
                try {
                    System.out.println("What do you want to do? \n1) View Products \n2) Order Product ");
                    whatToDo = keyboard.nextInt();
                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and 2");
                    keyboard.next();
                }
            }
            switch (whatToDo) {
                case 1:
                    dataManager.viewData(data);
                    break;
                case 2:
                    dataManager.buyElectronic(data, user, conn);
                    break;
            }
        }
    }

    /**
     * loginOrCreate asks for user to login or create an account as a user. This involves all the asking for inputs.
     * @param conn - connection to database
     */
    public void loginOrCreate(Connection conn){
        
        try (Scanner keyboard = new Scanner(System.in)) {
            int whatToDo = 0;
            while(!(whatToDo == 1 || whatToDo == 2)){
                try {
                    System.out.println("\n==== Welcome to the Electronik warehouse :D ====\n------------------------------------------------ \n\nPlease select one of the options: \n  1) Login \n  2) Create an account");
                    whatToDo = keyboard.nextInt();

                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and 2");
                    keyboard.next();
                }
            }

            switch (whatToDo) {
                case 1:
                    System.out.println("\nPlease enter your username :");
                    String usn = keyboard.next();
                    System.out.println("\nPlease enter your password :");
                    String pwd = keyboard.next();
                    User oldUser = new User(usn, pwd);
                    if(oldUser.validateUser(conn, usn, pwd)){

                        IOloadProducts newIOloadProducts = new IOloadProducts();
                        List<Electronics> list = newIOloadProducts.loadElectronics();
                        Application app = new Application();
                        AdminApplication adminapp = new AdminApplication();
                        if(oldUser.checkAdmin(conn, usn)){
                            Admin oldAdmin = new Admin(usn, pwd);
                            adminapp.showOptionsAdmin(list, oldAdmin, conn);
                        }else {
                            app.showOptions(list, oldUser, conn);
                        }
                    }
                    break;
            
                case 2:
                    String newOrNot = "";
                    System.out.println("\nPlease enter your username:");
                    String newUsn = keyboard.next();
                    System.out.println("\nPlease enter your password:");
                    String newPwd = keyboard.next();
                    while(!(newOrNot.equals("yes") || newOrNot.equals("no"))){
                        System.out.println("\nAre you a new user? yes or no");
                        newOrNot = keyboard.next();
                    }
                    boolean bolNewOrNot = newOrNot.equals("yes") ? true : false;           

                    User newUser = new User(newUsn, newPwd);
                    newUser.createUser(conn, bolNewOrNot);
                    System.out.println("Account created successfully!");
                    break;
            }
        }
    }
}
