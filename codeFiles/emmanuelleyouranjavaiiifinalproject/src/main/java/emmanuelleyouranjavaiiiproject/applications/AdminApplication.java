package emmanuelleyouranjavaiiiproject.applications;
import java.sql.Connection;
import java.util.List;
import java.util.Scanner;
import emmanuelleyouranjavaiiiproject.dataManagers.DataManager;
import emmanuelleyouranjavaiiiproject.types.Electronics;
import emmanuelleyouranjavaiiiproject.types.Admin;

/**
 * AdminApplication class contains the showOptionsAdmin method
 * 
 * @author You Ran Wang
 * @version 2023/12/01
 */
public class AdminApplication{
    /**
     * showOptionsAdmin shows the admin all the things they can do and invokes the appropriate functions
     * 
     * @param data is a list of data to be used
     * @param admin is the admin using the application
     * @param conn is the connection that will be used to edit users and such in sql
     */
    public void showOptionsAdmin(List<Electronics> data, Admin admin, Connection conn){
        DataManager dataManager = new DataManager();
        try (Scanner keyboard = new Scanner(System.in)) {
            System.out.println("\n== You have " + admin.getPointsFromDB(conn, admin.getUserName()) + " points :) ==\n");
            int whatToDo = 69420;
            while(whatToDo < 1 || whatToDo > 7){
                try {
                    System.out.println("What do you want to do? \n1) View Products \n2) Order Product \n3) Add Product \n4) Edit Products \n5) Create Admin \n6) Edit User \n7) Delete User");
                    whatToDo = keyboard.nextInt();
                } catch (Exception e) {
                    System.out.println("Try again. You must input a number between 1 and 7");
                    keyboard.next();
                }
            }
            switch (whatToDo) {
                case 1:
                    dataManager.viewData(data);
                    break;
                case 2:
                    admin.twoTimesPoints(conn, admin);
                    dataManager.buyElectronic(data, admin, conn);
                    break;
                case 3:
                    dataManager.addProductOptions(data);
                    break;
                case 4:
                    dataManager.editProductOptions(data, keyboard);
                    break;
                case 5:
                    System.out.println("\nPlease enter your username:");
                    String newUsn = keyboard.next();
                    System.out.println("\nPlease enter your password:");
                    String newPwd = keyboard.next();
                    String newOrNot = "";
                    while(!(newOrNot.equals("yes") || newOrNot.equals("no"))){
                        System.out.println("new User? yes or no");
                        newOrNot = keyboard.next();
                    }
                    Admin newAdmin = new Admin(newUsn, newPwd);

                    boolean bolNewOrNot = newOrNot.equals("yes") ? true : false;
                    newAdmin.createUser(conn, bolNewOrNot);
                    break;
                case 6:
                    admin.displayUsers(conn);
                    int numToChange = 0;
                    String replace = "";
                    System.out.println("Please enter the user you wish to update: ");
                    String userToChange = keyboard.next();
                    int columnToChange = 0;
                    while(columnToChange < 1 || columnToChange > 3){
                        System.out.println("Please enter the column you wish to update: \n 1) username \n 2) password \n 3) points");
                        columnToChange= keyboard.nextInt();
                    }
                        
                    if(columnToChange == 3){
                        System.out.println("Please enter the number of points you wish to change to: ");
                        numToChange = keyboard.nextInt();
                    } else{
                        System.out.println("Please enter what you wish to replace it with: ");
                        replace = keyboard.next();
                    }

                    admin.updateUser(conn, userToChange, columnToChange, numToChange, replace);
                    break;
                case 7:
                    admin.displayUsers(conn);
                    System.out.println("Please enter the username of the user you wish to delete: ");
                    String userToDelete = keyboard.next();
                    admin.deleteUser(conn, userToDelete);
                    break;
            }
        }
    }
}
