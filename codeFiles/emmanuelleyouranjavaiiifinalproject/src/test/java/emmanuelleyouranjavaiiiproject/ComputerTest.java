package emmanuelleyouranjavaiiiproject;

import static org.junit.Assert.*;
import org.junit.Test;

import emmanuelleyouranjavaiiiproject.types.Computer;

/**
 * Unit testing for Computer class
 * @author Emmanuelle Lin
 * @version 2023/11/10
 */
public class ComputerTest {
    
    /**
     * This method tests if the constructor and the getters are working properly and it asserts false if one of them doesn't work.
     */
    @Test
    public void testConstructor(){

        Computer test = new Computer(500, "Delle", "DO-MD2233", 300, "QuantumCore XZ-9000", "NebulaForce GTX 3080X", 16, "HyperWatt Fusion 1000W Gold", "GalacticTower ATX Mid Tower", "CelestialTech Zephyr Z590 Pro");
        
        if(!(test.getPrice() == 500)){
            assertTrue(false);
        }
        if(!(test.getBrand() == "Delle")){
            assertTrue(false);
        }
        if(!(test.getModel_or_Name() == "DO-MD2233")){
            assertTrue(false);
        }
        if(!(test.getStorage() == 300)){
            assertTrue(false);
        }
        if(!(test.getCpu() == "QuantumCore XZ-9000")){
            assertTrue(false);
        }
        if(!(test.getGpu() == "NebulaForce GTX 3080X")){
            assertTrue(false);
        }
        if(!(test.getMemory() == 16)){
            assertTrue(false);
        }
        if(!(test.getPsu() == "HyperWatt Fusion 1000W Gold")){
            assertTrue(false);
        }
        if(!(test.getForm_factor() == "GalacticTower ATX Mid Tower")){
            assertTrue(false);
        }
        if(!(test.getMotherboard() == "CelestialTech Zephyr Z590 Pro")){
            assertTrue(false);
        }
    }

    /**
     * This method tests the setters and if they aren't setting the fields correctly, they'll fail.
     */
    @Test
    public void testAllSetters(){

        Computer test = new Computer(0, null, null, 0, null, null, 0, null, null, null);
        test.setPrice(900);
        test.setBrand("HPP");
        test.setModel_or_Name("QuantumTech NebulaPro X-1");
        test.setStorage(66);
        test.setCpu("QuantumFlare Z-7000X");
        test.setGpu("NovaStrike RTX 3070Ti");
        test.setMemory(500);
        test.setPsu("FusionPower 850W Platinum");
        test.setForm_factor("NebulaCube Micro ATX");
        test.setMotherboard("HyperTech NebulaBlast B450M");

        if(!(test.getPrice() == 900)){
            assertTrue(false);
        }
        if(!(test.getBrand().equals("HPP"))){
            assertTrue(false);
        }
        if(!(test.getModel_or_Name().equals("QuantumTech NebulaPro X-1"))){
            assertTrue(false);
        }
        if(!(test.getStorage() == 66)){
            assertTrue(false);
        }
        if(!(test.getCpu().equals("QuantumFlare Z-7000X"))){
            assertTrue(false);
        }
        if(!(test.getGpu().equals("NovaStrike RTX 3070Ti"))){
            assertTrue(false);
        }
        if(!(test.getMemory() == 500)){
            assertTrue(false);
        }
        if(!(test.getPsu().equals("FusionPower 850W Platinum"))){
            assertTrue(false);
        }
        if(!(test.getForm_factor().equals("NebulaCube Micro ATX"))){
            assertTrue(false);
        }
        if(!(test.getMotherboard().equals("HyperTech NebulaBlast B450M"))){
            assertTrue(false);
        }
        assertTrue(true);
    }

    /**
     * This method tests if the ToString method is working properly or not. 
     */
    @Test
    public void testToString(){
        
        Computer test = new Computer(500, "Dell", "DO-MD2233", 300, "QuantumCore XZ-9000", "NebulaForce GTX 3080X", 16, "HyperWatt Fusion 1000W Gold", "GalacticTower ATX Mid Tower", "CelestialTech Zephyr Z590 Pro");
        
        assertEquals("Price: 500.0, Brand: Dell, Model or Name: DO-MD2233, Storage: 300, CPU: QuantumCore XZ-9000, GPU: NebulaForce GTX 3080X, Memory: 16, PSU: HyperWatt Fusion 1000W Gold, Form Factor: GalacticTower ATX Mid Tower, Motherboard: CelestialTech Zephyr Z590 Pro",test.toString());
        
    }
}
