package emmanuelleyouranjavaiiiproject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import emmanuelleyouranjavaiiiproject.types.Phone;

/**
 * Unit test for phone class
 * @author You Ran Wang
 * @version 2023/11/10
 */
public class phoneTest 
{
    /**
     * getter test which fails if any of the getter return the wrong value also fail when constructor is not working properly
     */
    @Test
    public void getterConstructorTestShouldAnswerWithTrue()
    {
        Phone testPhone  = new Phone(69.0, "Apple", "Iphone XR", 69, "Black", "69X69 inches");
        if (!(testPhone.getPrice() == 69.0)) {
            assertTrue(false);
        }
        if (!(testPhone.getBrand() == "Apple")) {
            assertTrue(false);
        }
        if (!(testPhone.getModel_or_Name() == "Iphone XR")) {
            assertTrue(false);
        }
        if (!(testPhone.getStorage() == 69)) {
            assertTrue(false);
        }
        if (!(testPhone.getColor() == "Black")) {
            assertTrue(false);
        }
        if (!(testPhone.getSize() == "69X69 inches")) {
            assertTrue(false);
        }

        assertTrue( true );
    }

    /**
     * setter for constructor test which fails if setter fail to modify incomplete phone also fails when constructor doesnt work
     */
    @Test
    public void setterConstructorTestShouldAnswerWithTrue()
    {
        Phone testPhone  = new Phone(68.0, "Appl", "Iphone X", 68, "Blac", "69X69 inche");
        testPhone.setPrice(testPhone.getPrice() + 1.0);
        testPhone.setBrand(testPhone.getBrand() + "e");
        testPhone.setModel_or_Name(testPhone.getModel_or_Name() + "R");
        testPhone.setStorage(testPhone.getStorage() + 1);
        testPhone.setColor(testPhone.getColor() + "k");
        testPhone.setSize(testPhone.getSize() + "s");


        if (!(testPhone.getPrice() == 69.0)) {
            assertTrue(false);
        }
        if (!(testPhone.getBrand().equals("Apple"))) {
            assertTrue(false);
        }
        if (!(testPhone.getModel_or_Name().equals("Iphone XR"))) {
            assertTrue(false);
        }
        if (!(testPhone.getStorage() == 69)) {
            assertTrue(false);
        }
        if (!(testPhone.getColor().equals("Black"))) {
            assertTrue(false);
        }
        if (!(testPhone.getSize().equals("69X69 inches"))) {
            assertTrue(false);
        }
        assertTrue( true );
    }

    /**
     * test for the toString
     */
    @Test
    public void toStringTest(){
        Phone testPhone  = new Phone(68.0, "Appl", "Iphone X", 68, "Blac", "69X69 inche");
        assertEquals("Price: 68.0, Brand: Appl, Model or Name: Iphone X, Storage: 68GB, Color: Blac, Size: 69X69 inche", testPhone.toString());
    }
}
